var express = require('express');

var server = express(),
	port = 7847;

server.listen(port, function() {
	console.log("Server listening on port " + port);
});

server.get('/', function(req, res){
	console.log("Made a request");
	res.sendStatus(200);
});

server.use(express.static(__dirname + '/'));